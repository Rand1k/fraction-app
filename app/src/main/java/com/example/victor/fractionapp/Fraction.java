package com.example.victor.fractionapp;

/**
 * Created by victor on 27.10.16.
 */
public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public Fraction add (Fraction f) {
        if(denominator == f.denominator){
            return new Fraction((f.nominator + nominator ),( denominator));
        }
        if((nominator * f.denominator)+ (f.nominator * denominator)<0 & denominator * f.denominator < 0 ){
            return new Fraction((nominator * f.denominator)+ (f.nominator * denominator)*-1,( denominator * f.denominator)*-1);
        }
        return new Fraction((nominator * f.denominator)+ (f.nominator * denominator),( denominator * f.denominator));
    }


    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction sub(Fraction f2) {
        if(denominator == f2.denominator){
            return new Fraction((f2.nominator - nominator ),( denominator));
        }
        return new Fraction((nominator * f2.denominator)-(f2.nominator * denominator),(denominator* f2.denominator));
    }

    public Fraction mult(Fraction f2) {
        return new Fraction((nominator*f2.nominator),(denominator* f2.denominator));
    }

    public Fraction div(Fraction f2) {
        if (nominator * f2.denominator<0 & denominator * f2.nominator<0 ){
            return new Fraction((nominator * f2.denominator)*-1,(denominator* f2.nominator)*-1);
        }
        return new Fraction((nominator * f2.denominator),(denominator* f2.nominator));
    }
}
